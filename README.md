# API-AI For Conductor
An app that received webhook requests from API.AI and triggers an action based on the message received. Example of an action can be to look up a song on Spotify and send a request to Conductor server to play that song.

## Setup Instructions
- Open the file imports/configuration/config.js and add the api keys to the services that you want to use.
- Double click on "start.bat" to start the application.
- Make a port forward in your router from port 443 (https) to port 443 on your local machine.
- Make a port forward in your router from port 80 (http) to port 80 on your local machine.
- Install WAMP (or only install Apache if you prefer).
- Follow this guide to create a SSL certificate https://commaster.net/content/how-setup-lets-encrypt-apache-windows.
- Login to API.AI and enable Webhook and set the url to "https://your.domain.here.com/api-ai/interpret" and select "Enable webhook for all domains".
- Click on "Intents" in API.AI and select the intent that you want to be forwarded to the app and at the bottom of the page select "Use webhook" and "Use weebhook for slot-filling".

When you are done your Apache config should look like this:

```
<VirtualHost *:80>
	ServerName www.your.domain.com
	ServerAlias your.domain.com

	<Directory "c:/wamp/www">
		Order allow,deny
		Allow from all
		Require all granted
	</Directory>
</VirtualHost>

<VirtualHost *:443>
	ServerName www.your.domain.com
	ServerAlias your.domain.com
	
    SSLEngine on
    SSLCertificateFile "C:\Users\YourUserName\AppData\Roaming\letsencrypt-win-simple\httpsacme-v01.api.letsencrypt.org\your.domain.com-crt.pem"
    SSLCertificateKeyFile "C:\Users\YourUserName\AppData\Roaming\letsencrypt-win-simple\httpsacme-v01.api.letsencrypt.org\your.domain.com-key.pem"
	SSLCertificateChainFile "C:\Users\YourUserName\AppData\Roaming\letsencrypt-win-simple\httpsacme-v01.api.letsencrypt.org\your.domain.com-chain.pem"

    ProxyPass /api-ai http://localhost:5500/api-ai/
    ProxyPassReverse /api-ai http://localhost:5500/api-ai/

    ErrorLog "logs/error.log"
    CustomLog "logs/access.log" common
</VirtualHost>
```