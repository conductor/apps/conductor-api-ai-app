import { Meteor } from 'meteor/meteor';
import '../imports/utils/string.js';
import '../imports/collections/collections.js';
import '../imports/conductor/conductor.js';
import '../imports/routes.js';
import { log } from '../imports/logger/logger.js'

log.info("Starting application.");