import { IntegrationFactory } from './integration-factory.js'
import { ActionFactory } from './action-factory.js'
import { User } from '../user/user.js'
import { log } from '../logger/logger.js'

var AI = {

    interpret: function(message, callback) {
        var integrationName = message.originalRequest.source;
        var integrationData = message.originalRequest.data;
        var integration = IntegrationFactory.getIntegration(integrationName);

        if (integration == null) {
            log.warn('Unsupported integration "%s".', integrationName, integrationData);
            callback(integrationName + ' is not supported.');
            return;
        }

        var userId = integration.getUserId(integrationData);
        var user =  User.getUser(integration.getName(), userId);

        if (user == null) {
            User.createInvite(integration.getName(), userId, callback);
            return;
        }

        var actionName = message.result.action;
        var action = ActionFactory.getAction(actionName);

        if (action == null) {
            log.warn('Unsupported action "%s".', actionName);
            callback('Unsupported action "' + actionName + '".');
            return;
        }

        action.trigger(message, callback);
    }

}

export { AI }