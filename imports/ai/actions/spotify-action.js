import { Spotify } from '../../spotify/spotify.js';
import { log } from '../../logger/logger.js'

var SpotifyAction = {

    trigger: function(message, callback) {
        var artist = message.result.parameters['music-artist'];
        var track = message.result.parameters['music-track'];

        log.info("The AI interpreted the message as a Spotify track with the artist '%s' and the track '%s'. Searching Spotify for this track...", artist, track);

        SpotifyAction.lookupSpotifyTrack(artist, track, function(error, trackUri) {
            if (error) {
                callback(error);
                return;
            }

            log.info("Spotify found the track uri '%s' for the artist '%s' and track '%s':", trackUri, artist, track);

            SpotifyAction.changeSpotifyTrack(trackUri, function(error) {
                if (error) {
                    callback(error);
                    return;
                }

                log.info('Spotify track was successfully changed.');
                callback(null, null);
            });
        });
    },

    lookupSpotifyTrack: function(artist, track, callback) {
        Spotify.getTrackURI(artist, track, function(error, trackUri) {
            if (error) {
                log.error('Error recieved from spotify when looking up trackuri for %s - %s.', artist, track, error);
                callback('Error recieved from spotify when looking up trackuri for {0} - {1}.'.format(artist, track));
            } else if (trackUri) {
                callback(null, trackUri);
            } else {
                log.info('No spotify track found for %s - %s', artist, track);
                callback('No spotify track found for ' + artist + ' - ' + track);
            }
        });
    },

    changeSpotifyTrack: function(trackUri, callback) {
        log.info('Sending request to change spotify track to {0}.'.format(trackUri));

        Conductor.setPropertyValue({ propertyName: 'trackURI' }, trackUri, function(error, result) {
            if (error) {
                log.error('An error occured when sending "set property value" request to conductor: %s.', error);
                callback('An error occured when sending "set property value" request to conductor: {0}.'.format(error));
            } else {
                callback();
            }
        });
    }

}

export { SpotifyAction }