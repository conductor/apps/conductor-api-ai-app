import { TelegramIntegration } from './integrations/telegram-integration.js'
import { GoogleIntegration } from './integrations/google-integration.js'
import { log } from '../logger/logger.js'

var integrations = {
    'telegram': TelegramIntegration,
    'google': GoogleIntegration
}

var IntegrationFactory = {

    getIntegration: function(integrationName) {
        log.debug('Looking up source "%s".', integrationName);
        return integrations[integrationName];
    }

}

export { IntegrationFactory }