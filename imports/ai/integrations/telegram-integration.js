import { log } from '../../logger/logger.js'

var TelegramIntegration = {

    getName: function() {
        return 'telegram';
    },

    getUserId: function(telegramMessage) {
        return telegramMessage.message.from.id;
    }

}

export { TelegramIntegration }