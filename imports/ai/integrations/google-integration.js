import { log } from '../../logger/logger.js'

var GoogleIntegration = {

    getName: function() {
        return 'google';
    },

    getUserId: function(googleMessage) {
        return googleMessage.user.userId;
    }

}

export { GoogleIntegration }