String.prototype.format = function() {
    var args = arguments;

    return this.replace(/\{(\d+)\}/g, function() {
        var argument = args[arguments[1]];

        if (isPrimitive(argument)) {
            return argument;
        } else {
            return JSON.stringify(argument);
        }
    });
};

function isPrimitive(test) {
    return (test !== Object(test));
};
