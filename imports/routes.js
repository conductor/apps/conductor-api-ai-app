import { log } from './logger/logger.js'
import { AI } from './ai/ai.js'

var bodyParser = require( 'body-parser');
Picker.middleware( bodyParser.json() );

Picker.route('/api-ai/interpret', function(params, req, res, next) {
    log.info('Incoming request.', req.body);

    try {
        AI.interpret(req.body, function(error, result) {
            var message = error ? error : result;
            respondWithMessage(message, res);
        });
    } catch (e) {
        log.error('AI failed to interpret request.', e.stack);
        respondWithMessage('Server error, try again later.', res);
    }
});

function respondWithMessage(message, response) {
    response.end(JSON.stringify({
      "speech": message,
      "display_text": message
    }));
}
