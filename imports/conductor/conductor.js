Conductor = require('node-conductor');
Conductor.connect(Meteor.settings.conductor.url);
Conductor.setApiKey(Meteor.settings.conductor.apiKey);
Conductor.subscribe(Conductor.Subscription.DEVICES);
Conductor.subscribe(Conductor.Subscription.SET_PROPERTY_VALUE_REQUEST_SUMMARIES);
Conductor.subscribe(Conductor.Subscription.APPLICATION_INVITES);
