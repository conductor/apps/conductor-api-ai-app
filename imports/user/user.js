import { log } from '../logger/logger.js'

var TinyURL = require('tinyurl');

Users.remove({});

var User = {

    getUser: function(integration, userId) {
        var query = {};
        var path = 'integrations.' + integration + '.userId'
        query[path] = userId.toString();
        return Users.findOne(query);
    },

    existUser: function(integration, userId) {
        return User.getUser(integration, userId) != null;
    },

    createInvite: function(integration, userId, callback) {
        Conductor.createInvite(integration + ':' + userId, function(error, result) {
            if (error) {
                log.error('An error occured when sending "createInvite" request to conductor.', error);
                callback('Welcome! We are experience some troubles right now and can\'t create an invitation for you, please try again later.');
            } else {
                log.info('Successfully created a new invite.', result);
                TinyURL.shorten(result.inviteUrl, Meteor.bindEnvironment(function(shortUrl, error) {
                    callback(null, 'Welcome! Please click the following link to accept the invitation ' + shortUrl + '.');
                }));
            }
        });
    },

    createUser: function(publicKey) {
        log.info('Creating a new user with the public key "%s".', publicKey);

        var newUser = {
            integrations: {
                conductor: {
                    publicKey: publicKey,
                    userId: publicKey
                }
            }
        };

        return Users.insert(newUser);
    },

    addIntegrationToUser: function(publicKey, integrationName, integrationUserId) {
        log.info('Adding integration "%s" with the integration user id "%s" to the user with public key "%s": ', integrationName, integrationUserId, publicKey);

        var existingUser = User.getUser('conductor', publicKey);
        var updateQuery = {
            '$set': { }
        }

        updateQuery['$set']['integrations.' + integrationName] = {
            userId: integrationUserId
        }

        Users.update({ _id: existingUser._id}, updateQuery);
    }

}

Conductor.Invites.find({}).observe({
    changed: function(invite, oldInvite) {
        log.debug('Invite changed.', invite);

        // If the user has accepted the application invite, then add/update the user.
        if (invite.userPublicKey && invite.applicationInviteStatus === 'ACCEPTED') {
            var existingUser = User.getUser('conductor', invite.userPublicKey);

            // Add a new user if no user is found with the public key.
            if (!User.existUser('conductor', invite.userPublicKey)) {
                existingUser = User.createUser(invite.userPublicKey);
            }

            // Add the integration user id to the user.
            var inviteInfo = parseExternalId(invite.externalId);
            User.addIntegrationToUser(invite.userPublicKey, inviteInfo.integration, inviteInfo.userId);
        }
    }
});

function parseExternalId(externalId) {
    return {
        integration: externalId.split(':')[0],
        userId: externalId.split(':')[1]
    }
}

export { User }