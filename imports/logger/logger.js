import winston from 'winston'
import winstonLogstash from 'winston-logstash'
import winstonLogzio from 'winston-logzio';

const log = winston;

winston.remove(winston.transports.Console);

if (Meteor.settings.logger.console.enable) {
    winston.add(winston.transports.Console, Meteor.settings.logger.console.options);
    log.info('Logging to console enabled.');
}

if (Meteor.settings.logger.file.enable) {
    winston.add(winston.transports.File, Meteor.settings.logger.file.options);
    log.info('Logging to file enabled.');
}

if (Meteor.settings.logger.logstash.enable) {
    winston.add(winston.transports.Logstash, Meteor.settings.logger.logstash.options);
    log.info('Logging to logstash enabled.');
}

if (Meteor.settings.logger.logzio.enable) {
    winston.add(winstonLogzio, Meteor.settings.logger.logzio.options);
    log.info('Logging to logzio enabled.');
}

function formatMessage(level, time, message, data, userId) {
    return '[' + level + '] ' +
        moment(time).format('YYYYMMDD-hh:mm:ss') +
        ' - ' +
        message + '\r\n' +
        (!isEmpty(data) ? toString(data) + '\r\n' : '') +
        ((userId != null) ? 'User: ' + userId + '\r\n' : '');
}

function toString(value) {
    if (value == null) {
        return null;
    } else if (isPrimitive(value)) {
        return value;
    } else {
        return JSON.stringify(value, null, 4);
    }
}

function isEmpty(obj) {
    if (obj == null) {
        return true;
    }

    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

function isPrimitive(test) {
    return (test !== Object(test));
};

export { log }